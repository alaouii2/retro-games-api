from app import app, db
from app.models import Game, Platform, Publisher, Genre, game_platform, game_genre, game_publisher
from flask import jsonify, redirect, request
from unicodedata import normalize

import random


@app.route('/', methods=['GET'])
def home():
    return jsonify(
        {
            "endpoints": ["/game/<id>", "/game/random", "/games", "/platforms", "/publishers", "/genres"]
        }
    )

@app.route('/games/', methods=['GET'])
def api_games():
    title = request.form.get("title")
    year = request.form.get("year")
    platform = request.form.get("platform")
    publisher = request.form.get("publisher")
    genre = request.form.get("genre")

    games_filtered = Game.query

    if title:
        games_filtered = games_filtered.filter(Game.title.like(f"%{title}%"))
    if year:
        games_filtered = games_filtered.filter(Game.year == int(year))
    if platform:
        games_filtered = games_filtered.filter(Game.platforms.any(Platform.name == platform))
    if publisher:
        games_filtered = games_filtered.filter(Game.publishers.any(Publisher.name == publisher))
    if genre:
        games_filtered = games_filtered.filter(Game.genres.any(Genre.name == genre))

    allg = games_filtered.limit(1000).all()

    gj = [{
        "id": g.id,
        "Title": g.title,
        "Year": g.year,
        "Platforms": [pl.name for pl in g.platforms],
        "Publisher": [pb.name for pb in g.publishers],
        "Genre": [gr.name for gr in g.genres],
    } for g in allg]
    return jsonify(gj)


@app.route('/game/<id>', methods=['GET'])
def api_id(id):
    g = Game.query.filter_by(id=id).first()

    gj = {
        "id": g.id,
        "Title": g.title,
        "Year": g.year,
        "Platforms": [pl.name for pl in g.platforms],
        "Publisher": [pb.name for pb in g.publishers],
        "Genre": [gr.name for gr in g.genres],
    }
    return jsonify(gj)

@app.route('/game/random', methods=['GET'])
def api_random():
    i = random.randint(1, int(Game.query.count()))
    return api_id(i)

@app.route('/platforms', methods=['GET'])
def api_platforms():
    name = request.form.get("name")

    platforms_filtered = Platform.query

    if name:
        platforms_filtered = platforms_filtered.filter(Platform.name.like(f"%{name}%"))

    allpl = platforms_filtered.limit(100).all()

    return jsonify([{
        "id": pl.id,
        "name": pl.name
    } for pl in allpl])

@app.route('/publishers', methods=['GET'])
def api_publishers():
    name = request.form.get("name")

    publishers_filtered = Publisher.query

    if name:
        publishers_filtered = publishers_filtered.filter(Publisher.name.like(f"%{name}%"))

    allpb = publishers_filtered.limit(100).all()

    return jsonify([{
        "id": pb.id,
        "name": pb.name
    } for pb in allpb])

@app.route('/genres', methods=['GET'])
def api_genres():
    name = request.form.get("name")

    genres_filtered = Genre.query

    if name:
        genres_filtered = genres_filtered.filter(Genre.name.like(f"%{name}%"))

    allgr = genres_filtered.limit(100).all()

    return jsonify([{
        "id": gr.id,
        "name": gr.name
    } for gr in allgr])
